# perlscripts
Various perl scripts that don't have a place but may be handy.

#### domainvalidate.pl 
Takes domains names in a newline format and prints to standard output the valid domains. I used this to sort out a 60,000 line list
of domain names for ad-blocking.

#### domainsort.pl
Takes stdin or a list of files and uses a specified DNS server to resolve the entries. Entries are then sorted and placed in "invalid.txt" or "valid.txt".
