#!/usr/bin/env perl

=for comment
Copyright <2017> <Wilyarti Howard>
 
Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentatio
n and/or other materials provided with the distribution.
 
3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software w
ithout specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE 
GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRIC
T LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SU
CH DAMAGE.
                
=cut

use strict;
use warnings;
use v5.20;
use feature qw(signatures);
no warnings qw(experimental::signatures);
use Net::DNS;

&main();

sub main() {
    # loop over STDIN or list of files
    while (<<>>) {
        # remove new line
        chomp($_);
        
        # run domainCheck subroutine
        # it returns true (1) if valid do it this way
        # so we don't create not needed variables
        if (&domainCheck($_)) {
            # say automaticaly prints a new line
            say $_;
        }
    }
}

sub domainCheck ($domain) {
    # instantiate our resolver object
    my $r = Net::DNS::Resolver->new;

    # specify time out
    $r->udp_timeout(2);
    $r->tcp_timeout(2);

    # return true if domain is valid
    if ($r->search($domain)) {
        return 1;
    } else {
        return 0;
    }
}
